import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

import modules from '@/store/modules'

Vue.use(Vuex);

const persistedStateOptions = {
    paths: [
        'Auth.user',
        'Auth.profile',
        'Auth.clients',
        'Auth.tokens',
        'Auth.roles',
        'Auth.permissions',
        'Auth.apiConfig',
    ],
    reducer: (val) => {
        if(val.Auth.tokens.access.value === null) { // return empty state when user logged out
            return {}
        }
        return val
    }
};

export default new Vuex.Store({
    modules: modules,
    plugins: [
        createPersistedState(persistedStateOptions)
    ],
    state: {

    },
    mutations: {

    },
    actions: {

    },
    getters: {

    }
})