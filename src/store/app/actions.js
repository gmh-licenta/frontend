export default {
    toggleDrawer ({ state, commit }) {
        return new Promise((resolve, reject) => {
            commit('toggleDrawer');
            resolve();
        });
    },
    setDrawer ({ state, commit }, payload) {
        return new Promise((resolve, reject) => {
            commit('setDrawer', payload);
            resolve();
        });
    }
}