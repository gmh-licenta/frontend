export default {
    toggleDrawer (state) {
        state.drawer = !state.drawer;
    },
    setDrawer (state, payload) {
        state.drawer = payload.drawer;
    },
}