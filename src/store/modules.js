import AuthModule from './auth/module'
import AppModule from "./app/module"

export default {
    Auth: AuthModule,
    App: AppModule,
}