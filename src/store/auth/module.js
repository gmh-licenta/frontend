// import state from '@/store/auth/state'
// import mutations from '@/store/auth/mutations'
// import actions from '@/store/auth/actions'
// import getters from '@/store/auth/getters'

import { cloneObject } from '@/mixins/helpers';
import httpStatusCodes from '@/global/constants/httpStatusCodes'
import store from "@/store";


const initialState = {
    user: {},
    profile: {},
    clients: {
        personalAccess: null,
        passwordGranted: null,
    },
    tokens: {
        access: {
            type: null,
            value: null,
            expires_in: null,
        },
        refresh: {
            type: null,
            value: null,
            expires_in: null,
        },
    },
    roles: [],
    permissions: {
        view: [],
        action: [],
    },
    apiConfig: {},
    temporaryCredentials: null,
};

Object.freeze(initialState);

export default {
    namespaced: true,
    state: cloneObject(initialState),
    mutations: {
        // mutations,
        authenticate(state, payload)
        {
            state.tokens.access.value = payload.tokens.access_token;
            state.tokens.access.type = payload.tokens.token_type;
            state.tokens.access.expires_in = payload.tokens.expires_in;
            state.tokens.refresh.value = payload.tokens.refresh_token;
            state.tokens.refresh.type = payload.tokens.token_type;
            state.tokens.refresh.expires_in = payload.tokens.expires_in;

            state.user = payload.user;
            state.profile = payload.profile;

            state.roles = payload.roles;

            state.permissions.view = payload.permissions.view;
            state.permissions.action = payload.permissions.action;

            state.apiConfig = payload.apiConfig;
            state.apiConfig.initialized = (state.apiConfig.initialized === 'true' || state.apiConfig.initialized === true);
        },
        storeClients(state, payload)
        {
            state.clients = payload;
        },
        reset(state)
        {
            const temporaryCredentials = state.temporaryCredentials ?? null;
            Object.assign(state, cloneObject(initialState));
            if (temporaryCredentials !== null)
            {
                state.temporaryCredentials = temporaryCredentials;
            }
        },
        updateUserData(state, payload)
        {
            state.user.name = payload.name;
            state.user.email = payload.email;
        },
        updateUserProfile(state, payload)
        {
            state.profile = payload.profile;
        },
        updateApiConfig(state, payload)
        {
            state.apiConfig = payload.config;
            state.apiConfig.initialized = true;
            state.apiConfig.country_code = cloneObject(state.apiConfig).country;
            state.apiConfig.country = payload.supportedCountries.find(c => c.code === state.apiConfig.country_code).country;
        },
        apiInitialized(state, payload)
        {
            state.apiConfig.initialized = true;
        },
        storeTemporaryCredentials(state, payload)
        {
            state.temporaryCredentials = payload;
        },
        deleteTemporaryCredentials(state, payload)
        {
            state.temporaryCredentials = null;
        },
        updateTokens(state, payload)
        {
            state.tokens.access.value = payload.tokens.access_token;
            state.tokens.access.type = payload.tokens.token_type;
            state.tokens.access.expires_in = payload.tokens.expires_in;
            state.tokens.refresh.value = payload.tokens.refresh_token;
            state.tokens.refresh.type = payload.tokens.token_type;
            state.tokens.refresh.expires_in = payload.tokens.expires_in;
        }
    },
    actions: {
        authenticate({state, commit}, payload)
        {
            return new Promise((resolve, reject) => {
                commit('authenticate', payload);
                resolve();
            });
        },
        logout({state, commit, getters})
        {
            return new Promise((resolve, reject) => {
                if (getters.userAuthenticated)
                {
                    const payload = {
                        clientId: state.clients.passwordGranted.id,
                        userId: state.user.id,
                    };
                    this._vm.$axios.api.post('/api/oauth/auth/logout', payload)
                        .then( (response) => {
                            commit('reset');
                            if (response.status === httpStatusCodes.HTTP_RESET_CONTENT)
                            {
                                resolve();
                            }
                            else
                            {
                                reject();
                            }
                        });
                }
                else
                {
                    commit('reset');
                    resolve();
                }
            });
        },
        reset({state, commit, getters})
        {
            return new Promise((resolve, reject) => {
                commit('reset');
                resolve();
            });
        },
        async tokenAlive({state, commit})
        {
            return await new Promise((resolve, reject) => {
                const payload = {
                    clientId: state.clients.passwordGranted.id,
                    userId: state.user.id,
                };
                return this._vm.$axios.api.post('/api/oauth/token/check', payload)
                    .then( (response) => {
                        if (response.status === httpStatusCodes.HTTP_NO_CONTENT)
                        {
                            resolve(true);
                        }
                        else
                        {
                            reject();
                        }
                    })
                    .catch( (error) => {
                        if (error.response.status === httpStatusCodes.HTTP_UNAUTHORIZED)
                        {
                            resolve(false);
                        }
                        else
                        {
                            reject();
                        }
                    });
            });
        },
        async refreshToken({state, commit})
        {
            return await new Promise((resolve, reject) => {
                const payload = {
                    clientId: state.clients.passwordGranted.id,
                    userId: state.user.id,
                    refreshToken: state.tokens.refresh.value,
                };
                return this._vm.$axios.api.post('/api/oauth/token/refresh', payload)
                    .then( (response) => {
                        if (response.status === httpStatusCodes.HTTP_OK)
                        {
                            commit('updateTokens', response.data);
                            resolve(true);
                        }
                        else
                        {
                            resolve(false);
                        }
                    })
                    .catch( (error) => {
                        resolve(false);
                    });
            });
        },
        async getApiClients({state, commit})
        {
            return await new Promise((resolve, reject) => {
                let payload = {
                    passwordGranted: null,
                    personalAccess: null,
                };

                const passwordGrantedClientParameterBag = {
                    name: 'API Password Grant Client',
                    user_id: null,
                    confidential: true,
                    personal_access_client: false,
                    password_client: true,
                    redirect: process.env.VUE_APP_BASE_URL,
                };
                this._vm.$axios.api.post('/api/oauth/client/find-or-fail', passwordGrantedClientParameterBag)
                    .then( (response) => {
                        if (response.data.client === null)
                        {
                            this._vm.$axios.api.post('/api/oauth/client', passwordGrantedClientParameterBag)
                                .then( (response) => {
                                    payload.passwordGranted = response.data.client;
                                })
                            ;
                        }
                        else
                        {
                            payload.passwordGranted = response.data.client;
                        }
                    })
                    .then( () => {

                        const personalAccessClientParameterBag = {
                            name: 'API Personal Access Client',
                            user_id: null,
                            confidential: true,
                            personal_access_client: true,
                            password_client: false,
                            redirect: process.env.VUE_APP_BASE_URL,
                        };

                        this._vm.$axios.api.post('/api/oauth/client/find-or-fail', personalAccessClientParameterBag)
                            .then( (response) => {
                                if (response.data.client === null)
                                {
                                    this._vm.$axios.api.post('/api/oauth/client', personalAccessClientParameterBag)
                                        .then( (response) => {
                                            payload.personalAccess = response.data.client;
                                        })
                                    ;
                                }
                                else
                                {
                                    payload.personalAccess = response.data.client;
                                }
                            })
                            .then( async () => {
                                await store.dispatch('Auth/storeClients', payload)
                                    .then( (response) => {
                                        resolve();
                                    })
                                ;
                            })
                        ;
                    })
                ;
            });
        },
        async storeClients({state, commit}, payload)
        {
            return await new Promise((resolve, reject) => {
                commit('storeClients', payload);
                resolve();
            });
        },
        updateUserData({state, commit}, payload)
        {
            return new Promise((resolve, reject) => {
                commit('updateUserData', payload);
                resolve();
            });
        },
        apiInitialized({state, commit}, payload)
        {
            return new Promise((resolve, reject) => {
                commit('apiInitialized');
                resolve();
            });
        },
        refreshUserProfile({state, commit})
        {
            return new Promise( (resolve, reject) => {
                return this._vm.$axios.api.get('/api/user-profile/get')
                    .then( (response) => {
                        commit('updateUserProfile', response.data);
                        resolve();
                    });
            });
        },
        refreshApiConfig({state, commit})
        {
            return new Promise( (resolve, reject) => {
                return this._vm.$axios.api.get('/api/config/get')
                    .then( (response) => {
                        commit('updateApiConfig', response.data);
                        resolve();
                    });
            });
        },
        getUserProfile({state, commit})
        {
            return new Promise( (resolve, reject) => {
                return this._vm.$axios.api.get('/api/user-profile/get')
                    .then( (response) => {
                        resolve(response);
                    });
            });
        },
        storeTemporaryCredentials({state, commit}, payload)
        {
            return new Promise( (resolve, reject) => {
                commit('storeTemporaryCredentials', payload);
                resolve();
            });
        },
        deleteTemporaryCredentials({state, commit, getters}, payload)
        {
            return new Promise( (resolve, reject) => {
                commit('deleteTemporaryCredentials');
                resolve();
            });
        },
        // actions,
    },
    getters: {
        getAccessToken: (state) => state.tokens.access,
        userAuthenticated: (state) => state.tokens.access.value !== null,
        getPasswordGrantedClient: (state) => state.clients.passwordGranted,
        clientsSet: (state) => state.clients.passwordGranted !== null && state.clients.personalAccess !== null,
        getUser: (state) => state.user,
        getUserProfile: (state) => state.profile,
        getUserProfilePicture: (state, getters) => getters.getUserProfile.picture,
        getUserRoles: (state) => state.roles,
        getUserPermissions: (state) => state.permissions,
        getUserViewPermissions: (state) => state.permissions.view,
        getUserActionPermissions: (state) => state.permissions.action,
        isApiInitialized: state => state.apiConfig.initialized,
        getApiCountryCode: state => state.apiConfig.country_code,
        getApiCountry: state => state.apiConfig.country,
        isSuperAdmin: (state, getters) => getters.getUserRoles[0] === 'SuperAdmin',
        canDisplayAppLayout: (state, getters) => (getters.userAuthenticated && (getters.isApiInitialized || (!getters.isApiInitialized && getters.isSuperAdmin))),
        getTemporaryCredentials: state => state.temporaryCredentials,
        // getters,
    },
}