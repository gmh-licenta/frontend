import Vue from 'vue';

import { directive as clickOutside} from 'vue-clickaway';
import closeable from "@/directives/closeable";

Vue.directive('click-outside', clickOutside);
Vue.directive('closeable', closeable);