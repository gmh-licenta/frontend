import httpStatusCodes from '@/global/constants/httpStatusCodes'

let constants = {};

Object.assign(constants, httpStatusCodes);

Object.freeze(constants);

export default constants;
