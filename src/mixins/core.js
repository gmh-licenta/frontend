let Vue = null;

function setInstance(instance) {
    if (Vue === null)
    {
        Vue = instance;

        init();
    }
}

let appRoutes = [];

function init()
{
    setRoutes();
}
function setRoutes()
{
    appRoutes = Vue.$router.options.routes.filter(route => route.path === '/')[0].children;
}

function getCurrentRoute() {
    return Vue.$route;
}
function getCurrentRouteName() {
    return getCurrentRoute().name;
}

async function goTo(routeName, replace = false)
{
    // if (routeName === 'logout')
    // {
    //     await logout();
    //     routeName = 'login';
    //     Vue.$router
    //         .replace({
    //             name: routeName,
    //         })
    //         .catch(err => {})
    //     ;
    // }
    // else
    // {
    //     Vue.$router
    //         .push({
    //             name: routeName,
    //         })
    //         .catch(err => {})
    //     ;
    // }

    if (replace)
    {
        Vue.$router
            .replace({
                name: routeName,
            })
            .catch(err => {})
        ;
    }
    else
    {
        Vue.$router
            .push({
                name: routeName,
            })
            .catch(err => {})
        ;
    }


}

function logout()
{
    return Vue.$store.dispatch('Auth/logout');
}

function getRoutes()
{
    return appRoutes;
}

function getSidebarRoutes()
{
    return getRoutes().filter(route => route.sidebar === true);
}

function getSidebarRoutesNames()
{
    let routesNames = [];

    let routes = getSidebarRoutes();
    for (let route of routes)
    {
        routesNames.push(route.name);

        if (Object.getOwnPropertyDescriptor(route, 'children'))
        {
            for (let cRoute of route.children)
            {
                routesNames.push(cRoute.name);

                if (Object.getOwnPropertyDescriptor(cRoute, 'children'))
                {
                    for (let ccRoute of cRoute.children)
                    {
                        routesNames.push(ccRoute.name);

                        if (Object.getOwnPropertyDescriptor(ccRoute, 'children'))
                        {
                            for (let cccRoute of ccRoute.children)
                            {
                                routesNames.push(cccRoute.name);
                            }
                        }
                    }
                }
            }
        }
    }

    return routesNames;
    // return this.getSidebarRoutes().map(route => route.name);
}

export {
    setInstance,

    getCurrentRoute,
    getCurrentRouteName,
    goTo,
    getRoutes,
    getSidebarRoutes,
    getSidebarRoutesNames,
    logout,
};
