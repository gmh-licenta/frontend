let Vue = null;

function setInstance(instance)
{
    if (Vue === null)
    {
        Vue = instance;
    }
}

// Require `PhoneNumberFormat`.
const PNF = require('google-libphonenumber').PhoneNumberFormat;

// Get an instance of `PhoneNumberUtil`.
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return !(re.test(email));
}

function emptyString(str)
{
    return (str.length === 0 || str.trim().length === 0)
}

function validatePhoneNumber(phoneNumber, region = 'RO') {
    // Parse number with country code and keep raw input.
    try
    {
        const number = phoneUtil.parseAndKeepRawInput(phoneNumber, region);

        if (phoneUtil.isValidNumber(number) === true)
        {
            return {
                error: false,
                type: null,
            }
        }
        else
        {
            return {
                error: true,
                type: 'invalid_phone_number',
            }
        }
    }
    catch (error)
    {
        return {
            error: true,
            type: 'not_a_phone_number',
        };
    }
}

function exists(array, path, property, value)
{
    if (array.length === 0)
    {
        return false;
    }

    let found;

    if (path !== null)
    {
        if (path.includes("."))
        {
            path = path.split(".");
        }

        if (path instanceof Array)
        {
            found = array
                .filter(item =>
                    {
                        for (let nestedPath of path)
                        {
                            item = item[nestedPath];
                        }
                        return item[property] === value;
                    }
                )
                .length > 0;
        }
        else
        {
            found = array.filter(item => item[path][property] === value).length > 0;

        }
    }
    else
    {
        found = array.filter(item => item[property] === value).length > 0;
    }

    return found;
}

async function phoneNumberIsTaken(phoneNumber)
{
    const payload = {
        phone: phoneNumber,
    };
    return await Vue.$axios.api.post('api/user-profile/phone-number-is-taken', payload)
        .then( response => {
            return response.data.status;
        });
}

export {
    setInstance,

    validateEmail,
    emptyString,
    validatePhoneNumber,
    exists,
    phoneNumberIsTaken,
};