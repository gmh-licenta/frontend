import moment from 'moment'

// Require `PhoneNumberFormat`.
const PNF = require('google-libphonenumber').PhoneNumberFormat;

// Get an instance of `PhoneNumberUtil`.
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

function cloneObject(o)
{
    let out, v, key;
    out = Array.isArray(o) ? [] : {};
    for (key in o) {
        v = o[key];
        out[key] = (typeof v === "object" && v !== null) ? cloneObject(v) : v;
    }
    return out;
}

function firstLetterUpperCase(string)
{
    return string[0].toUpperCase() + string.slice(1);
}

function getUserInitials(name)
{
    let initials = "";

    let names = name.split(" ");

    for (let partialName of names) {

        let partialNames = partialName.split("-");

        for (let finalName of partialNames)
        {
            initials += finalName.charAt(0).toUpperCase();
        }
    }

    return initials;
}

function filterObject(obj, predicate)
{
    return Object.fromEntries(Object.entries(obj).filter(predicate));
}

function isDateBefore(date, dateToCompareAgainst)
{
    const parsedDate = moment(date).startOf('day');
    const compareDate = moment(dateToCompareAgainst).startOf('day');

    return compareDate > parsedDate;
}

function formatPhoneNumber(phoneNumber, region = 'RO', format = PNF.E164)
{
    const number = phoneUtil.parseAndKeepRawInput(phoneNumber, region);

    return phoneUtil.format(number, format);
}

export {
    cloneObject,
    firstLetterUpperCase,
    getUserInitials,
    filterObject,
    isDateBefore,
    formatPhoneNumber,
};