import { countries } from 'moment-timezone/data/meta/latest.json';

let Vue = null;

function setInstance(instance) {
    if (Vue === null)
    {
        Vue = instance;
    }
}

function setTimezoneByCountryCode(countryCode)
{
    let timezone;
    if (Object.prototype.hasOwnProperty.call(countries, countryCode))
    {
        timezone = countries[countryCode]['zones'][0];
        Vue.$moment.tz.setDefault(timezone);
    }
    else
    {
        for (let cc in countries)
        {
            if (cc.toLowerCase() === countryCode.toLowerCase())
            {
                timezone = countries[cc]['zones'][0];
                Vue.$moment.tz.setDefault(timezone);
                break;
            }
        }
    }
}


export {
    setInstance,

    setTimezoneByCountryCode,
};
