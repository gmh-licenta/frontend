import Vue from 'vue'
import App from './App.vue'

import router from '@/router'
import store from '@/store'

import vuetify from '@/plugins/vuetify'

import Swal from 'sweetalert2'

// import moment from 'moment'
import moment from 'moment-timezone'

import Croppa from 'vue-croppa';
import 'vue-croppa/dist/vue-croppa.css';

Vue.use(Croppa);

import '@/mixins';

import boot from "@/global/boot/boot";

import '@/directives/directives';

import LoadingSpinner from "@/components/LoadingSpinner";

Vue.component('loading-spinner', LoadingSpinner);

import draggable from 'vuedraggable';

Vue.component('draggable', draggable);

Vue.prototype.$moment = moment;
Vue.prototype.$swal = Swal;

let app;

boot(false).then( () => {
    app = new Vue({
        vuetify,
        router,
        store,
        render: h => h(App),
        async created() {
            this.setVueInstance(this);
            await boot(true);
            console.log('all initialization steps done, data arrived, vue mounted');
        }
    }).$mount('#app');
});