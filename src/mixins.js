import Vue from 'vue'

import * as Core from '@/mixins/core'
import CT from '@/mixins/constants'
import * as Validators from '@/mixins/validators'
import * as Helpers from '@/mixins/helpers'
import * as Timezone from '@/mixins/timezone'

function setVueInstance(instance)
{
    Core.setInstance(instance);
    Validators.setInstance(instance);
    Timezone.setInstance(instance);
}

let returnObj = {
    CT,
    Helpers,
    Validators,
    Timezone,
    setVueInstance,
};

for (let functionName of Object.keys(Core))
{
    if (functionName === 'setInstance')
    {
        continue;
    }

    returnObj[functionName] = Core[functionName];
}

export default Vue.mixin({
    data: () => {
        return returnObj;
    }
});
