import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'

import Swal from 'sweetalert2'

import Login from '@/views/pages/Login'

//layouts
import MainLayout from '@/views/layouts/MainLayout'

//page layouts
import L_Dashboard from '@/views/pages/Layouts/Dashboard'
import L_UserProfile from '@/views/pages/Layouts/UserProfile'
import L_Employees from '@/views/pages/Layouts/Employees'

//pages
import Dashboard from '@/views/pages/Dashboard'
import Clocking from '@/views/pages/Clocking'
import WorkFromHome from '@/views/pages/WorkFromHome'
import PersonalTimeOff from '@/views/pages/PersonalTimeOff'

//admin pages
import Departments from '@/views/pages/Departments'
import Positions from '@/views/pages/Positions'
import Employees from '@/views/pages/Employees'

//super admin pages
import InitializeApi from '@/views/pages/SuperAdmin/InitializeApi';
import LegalHolidays from '@/views/pages/LegalHolidays';
import Config from '@/views/pages/SuperAdmin/Config';
import AvailablePaidLeaveDays from "@/views/pages/SuperAdmin/AvailablePaidLeaveDays";
import TimeOffRequests from "@/views/pages/TimeOffRequests";
import Team from "@/views/pages/Team";
import DepartmentsHierarchy from "@/views/pages/SuperAdmin/DepartmentsHierarchy";
import PositionsHierarchy from "@/views/pages/SuperAdmin/PositionsHierarchy";

Vue.use(Router);

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/initialize-api',
            name: 'initialize_api',
            components: {
                InitializeApi: InitializeApi,
            },
            meta: {
                requiresAuth: true,
                requiresPermission: true,
            },
        },
        {
            path: '/login',
            name: 'login',
            meta: {
                requiresAuth: false,
                requiresPermission: false,
            },
            components: {
                Login: Login,
            }
        },
        {
            path: '/logout',
            name: 'logout',
            meta: {
                requiresAuth: true,
                requiresPermission: false,
            },
            beforeEnter: async (to, from, next) => {
                await store.dispatch('Auth/logout');
                console.log('user logged out');
                if (from.name !== 'login')
                {
                    next({
                        name: 'login',
                    });
                }
            },
        },
        {
            path: '/',
            meta: {
                requiresAuth: true,
                requiresPermission: true,
            },
            component: MainLayout,
            children: [
                {
                    path: 'user-profile',
                    name: 'user_profile',
                    component: L_UserProfile,
                },
                {
                    path: 'dashboard',
                    name: 'dashboard',
                    component: L_Dashboard,
                    sidebar: true,
                    icon: 'mdi-view-dashboard',
                    title: 'Dashboard',
                },
                {
                    path: 'clocking',
                    name: 'clocking',
                    component: Clocking,
                    sidebar: true,
                    icon: 'mdi-clock-outline',
                    title: 'Clocking',
                },
                {
                    path: 'work-from-home',
                    name: 'work_from_home',
                    component: WorkFromHome,
                    sidebar: true,
                    icon: 'mdi-home-city',
                    title: 'Work From Home',
                },
                {
                    path: 'personal-time-off',
                    name: 'personal_time_off',
                    component: PersonalTimeOff,
                    sidebar: true,
                    icon: 'mdi-calendar-month-outline',
                    title: 'Personal Time Off',
                },
                {
                    path: 'team',
                    name: 'team',
                    component: Team,
                    sidebar: true,
                    icon: 'mdi-account-group',
                    title: 'Team',
                },
                {
                    path: 'config',
                    name: 'config',
                    component: Config,
                    sidebar: true,
                    icon: 'mdi-settings',
                    title: 'Config',
                },
                {
                    path: 'available-paid-leave-days',
                    name: 'available_paid_leave_days',
                    component: AvailablePaidLeaveDays,
                    sidebar: true,
                    icon: 'mdi-calendar-plus',
                    title: 'Available Paid Leave Days',
                },
                {
                    path: 'legal-holidays',
                    name: 'legal_holidays',
                    component: LegalHolidays,
                    sidebar: true,
                    icon: 'mdi-calendar-star',
                    title: 'Legal Holidays',
                },
                {
                    path: 'time-off-requests',
                    name: 'time_off_requests',
                    component: TimeOffRequests,
                    sidebar: true,
                    icon: 'mdi-calendar-account',
                    title: 'Time Off Requests',
                },
                {
                    path: 'employees',
                    name: 'employees',
                    component: L_Employees,
                    sidebar: true,
                    icon: 'mdi-account-supervisor',
                    title: 'Employees',
                },
                {
                    path: 'departments',
                    name: 'departments',
                    component: Departments,
                    sidebar: true,
                    icon: 'mdi-account-group',
                    title: 'Departments',
                },
                {
                    path: 'positions',
                    name: 'positions',
                    component: Positions,
                    sidebar: true,
                    icon: 'mdi-account-details',
                    title: 'Positions',
                },
                {
                    path: 'departments-hierarchy',
                    name: 'departments.hierarchy',
                    component: DepartmentsHierarchy,
                    sidebar: true,
                    icon: 'mdi-account-group',
                    title: 'Departments Hierarchy',
                },
                {
                    path: 'positions-hierarchy',
                    name: 'positions.hierarchy',
                    component: PositionsHierarchy,
                    sidebar: true,
                    icon: 'mdi-account-group',
                    title: 'Positions Hierarchy',
                },
            ]
        },
        {
            path: '*',
            redirect: {
                name: 'dashboard',
            },
        },
    ]
});

router.beforeEach(async (to, from, next) => {

    console.log(from, to);
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

    const userAuthenticated = store.getters['Auth/userAuthenticated'];

    const isSuperAdmin = store.getters['Auth/isSuperAdmin'];
    const isApiInitialized = store.getters['Auth/isApiInitialized'];

    if (requiresAuth)
    {
        if (userAuthenticated)
        {
            if (isApiInitialized || (!isApiInitialized && isSuperAdmin))
            {
                if (!isApiInitialized && isSuperAdmin)
                {
                    if (to.name === 'initialize_api' || to.name === 'logout')
                    {
                        next();
                    }
                    else
                    {
                        next({
                            name: 'initialize_api',
                        });
                    }
                }
                else
                {
                    store.dispatch('Auth/refreshToken')
                        .then( (tokenAlive) => {
                            //true means the token is still available to be used
                            if (tokenAlive === true)
                            {
                                const requiresPermission = to.matched.some(record => record.meta.requiresPermission);

                                if (requiresPermission)
                                {
                                    const userHasPermissionToViewPage = store.getters['Auth/getUserViewPermissions']
                                        .some(page => page.includes(to.name))
                                    ;

                                    console.log(isApiInitialized, isSuperAdmin, requiresPermission, userHasPermissionToViewPage, userAuthenticated);
                                    if (userHasPermissionToViewPage)
                                    {
                                        next();
                                    }
                                    else
                                    {
                                        if (isSuperAdmin)
                                        {
                                            next({
                                                name: 'employees',
                                            });
                                        }
                                        else
                                        {
                                            next({
                                                name: 'dashboard',
                                            });
                                        }
                                        // if (from.path === to.path)
                                        // {
                                        //     if (isSuperAdmin)
                                        //     {
                                        //         next({
                                        //             name: 'employees',
                                        //         });
                                        //     }
                                        //     else
                                        //     {
                                        //         next({
                                        //             name: 'dashboard',
                                        //         });
                                        //     }
                                        // }
                                        // else
                                        // {
                                        //     if (from.name === null)
                                        //     {
                                        //         if (isSuperAdmin)
                                        //         {
                                        //             next({
                                        //                 name: 'employees',
                                        //             });
                                        //         }
                                        //         else
                                        //         {
                                        //             next({
                                        //                 name: 'dashboard',
                                        //             });
                                        //         }
                                        //     }
                                        //     else
                                        //     {
                                        //         next({
                                        //             name: from.name,
                                        //         });
                                        //     }
                                        // }
                                    }
                                }
                                else
                                {
                                    next();
                                }
                            }
                            //false means the user doesn't have a valid token (revoked or expired)
                            else
                            {
                                store.dispatch('Auth/reset')
                                    .then( response => {
                                        console.log('user logged out (session expired reset)');
                                        next({
                                            name: 'login',
                                        })
                                    });
                            }
                        })
                    ;
                }
            }
            else
            {
                if (to.name === 'logout')
                {
                    next();
                }
                else
                {
                    await Swal.fire({
                        title: 'The App Has Not Been Initialized!',
                        text: 'Please contact the Super Admin',
                        icon: 'error',
                        showCancelButton: true,
                        showConfirmButton: false,
                        cancelButtonText: 'Close',
                        cancelButtonColor: 'red',
                        allowEnterKey: false,
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                    })

                    next({
                        name: 'login',
                    });
                }
            }
        }
        else
        {
            next({
                name: 'login',
            })
        }
    }
    else
    {
        if (to.name === 'login' && userAuthenticated)
        {
            if (isSuperAdmin)
            {
                next({
                    name: 'employees',
                });
            }
            else
            {
                next({
                    name: 'dashboard',
                });
            }
        }
        else
        {
            next();
        }
    }
});


export default router;