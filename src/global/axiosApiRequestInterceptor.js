import store from '@/store';
import router from '@/router';
import Swal from 'sweetalert2'
import CT from '@/mixins/constants'


export default function bootAxiosRequestInterceptor(axiosGlobalInstance) {
    axiosGlobalInstance.interceptors.request.use(
        async function (config) {
            const userAuthenticated = store.getters['Auth/userAuthenticated'];

            if (userAuthenticated)
            {
                const token = store.getters['Auth/getAccessToken'];
                config.headers.Authorization = `${token.type} ${token.value}`;
            }

            return config;
        },
        function (err)
        {
            return Promise.reject(err);
        }
    );
    // Response interceptor
    axiosGlobalInstance.interceptors.response.use(
        async response => {
            if (response.status === 200)
            {
                if (response.data[1] === 401 && response.data[2]['X-Status-Reason'] === 'Access Denied')
                {
                    Swal.fire({
                        title: 'Access Denied',
                        icon: 'error',
                        showCancelButton: true,
                        showConfirmButton: false,
                        cancelButtonText: 'Close',
                        cancelButtonColor: 'red',
                        allowEnterKey: false,
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                    });

                    return Promise.reject('(401) Access Denied');
                }
            }

            return response;
        },
        async error => {
        const { status, headers, data } = error.response

        let title;
        if ('x-status-reason' in headers)
        {
            title = headers['x-status-reason'];
        }
        else
        {
            title = 'API Internal Error';
        }

        let message;
        if ('message' in data)
        {
            if (data.message === 'Unauthenticated.')
            {
                title = 'Session Expired';
                message = 'Please Login Again';
            }
            else if (data.message === 'User does not have the right roles.')
            {
                title = 'Access Denied';
                message = 'User does not have permission to perform this action.';
            }
            else
            {
                message = data.message;
            }
        }
        else
        {
            message = null;
        }

        if (status >= CT.HTTP_INTERNAL_SERVER_ERROR) {
            Swal.fire({
                title: title,
                icon: 'error',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: 'Close',
                cancelButtonColor: 'red',
                allowEnterKey: false,
                allowEscapeKey: false,
                allowOutsideClick: false,
            });
        }

        else if (status === CT.HTTP_UNAUTHORIZED) {
            Swal.fire({
                title: title,
                text: message,
                icon: 'error',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: 'Close',
                cancelButtonColor: 'red',
                allowEnterKey: false,
                allowEscapeKey: false,
                allowOutsideClick: false,
            });
        }

        else if (status === CT.HTTP_FORBIDDEN) {
            Swal.fire({
                title: title,
                text: message,
                icon: 'error',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: 'Close',
                cancelButtonColor: 'red',
                allowEnterKey: false,
                allowEscapeKey: false,
                allowOutsideClick: false,
            });
        }

        return Promise.reject(error)
    })
}
