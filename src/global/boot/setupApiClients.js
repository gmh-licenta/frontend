import store from '@/store'

export default function setupApiClients(axiosApiGlobalInstance) {
    return new Promise((resolve, reject) => {
        let payload = {
            passwordGranted: null,
            personalAccess: null,
        };

        const passwordGrantedClientParameterBag = {
            name: 'API Password Grant Client',
            user_id: null,
            confidential: true,
            personal_access_client: false,
            password_client: true,
            redirect: process.env.VUE_APP_BASE_URL,
        };
        axiosApiGlobalInstance.post('/api/oauth/client/find-or-fail', passwordGrantedClientParameterBag)
            .then( (response) => {
                if (response.data.client === null)
                {
                    axiosApiGlobalInstance.post('/api/oauth/client', passwordGrantedClientParameterBag)
                        .then( (response) => {
                            payload.passwordGranted = response.data.client;
                        })
                    ;
                }
                else
                {
                    payload.passwordGranted = response.data.client;
                }
            })
            .then( () => {

                const personalAccessClientParameterBag = {
                    name: 'API Personal Access Client',
                    user_id: null,
                    confidential: true,
                    personal_access_client: true,
                    password_client: false,
                    redirect: process.env.VUE_APP_BASE_URL,
                };

                axiosApiGlobalInstance.post('/api/oauth/client/find-or-fail', personalAccessClientParameterBag)
                    .then( (response) => {
                        if (response.data.client === null)
                        {
                            axiosApiGlobalInstance.post('/api/oauth/client', personalAccessClientParameterBag)
                                .then( (response) => {
                                    payload.personalAccess = response.data.client;
                                })
                            ;
                        }
                        else
                        {
                            payload.personalAccess = response.data.client;
                        }
                    })
                    .then( async () => {
                        await store.dispatch('Auth/storeClients', payload)
                            .then( (response) => {
                                resolve();
                            })
                            ;
                    })
                    ;
            })
            ;

    });
}