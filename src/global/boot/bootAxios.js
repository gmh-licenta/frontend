import Vue from 'vue';

import axios from "axios";
import bootAxiosRequestInterceptor from "@/global/axiosApiRequestInterceptor";

const axiosApiGlobalInstance = getAxiosApiInstance();

function getAxiosApiInstance()
{
    return axios.create({
        baseURL: `${process.env.VUE_APP_BACKEND_API_BASE_URL}`,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        }
    });
}

async function bootAxios()
{
    return await new Promise((resolve, reject) => {

        /*
        * instantiate 2 verios of axios, one for all-round requests and one for api
        *  requests with custom config
        * */

        bootAxiosRequestInterceptor(axiosApiGlobalInstance);
        Vue.prototype.$axios = axios;
        Vue.prototype.$axios.api = axiosApiGlobalInstance;

        resolve();
    });
}

export {
    bootAxios,
    axiosApiGlobalInstance,
};