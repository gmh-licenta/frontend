import { bootAxios, axiosApiGlobalInstance } from "@/global/boot/bootAxios";
import setupApiClients from "@/global/boot/setupApiClients";

async function bootComponents()
{
    await bootAxios();
    console.log("booted axios...");
    await setupApiClients(axiosApiGlobalInstance);
    console.log("booted api clients...");
}

// External function in module
function boot (vueCreated) {
    return new Promise((resolve, reject) => {
        switch (vueCreated) {
            case false: // "pre-init" initialization steps
                console.log('vue instance not created yet, booting app now...');

                bootComponents().then( () => { resolve(); } );

                break;
            case true: // we can continue/prepare data for Vue
                console.log('vue instance created, here post-init steps can be done...');

                resolve();

                break;
        }
    })
}

export default boot;