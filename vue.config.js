const path = require('path');

const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin');

module.exports = {
    configureWebpack: {
        plugins: [
            new VuetifyLoaderPlugin()
        ],
        resolve: {
            alias: {
                "@": path.resolve(__dirname, "src/"),
                "ROOT": path.resolve(__dirname),
            },
            aliasFields: ["@", "ROOT"]
        }
    },
};
